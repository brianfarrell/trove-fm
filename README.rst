
=====================
Trove Farmer's Market
=====================

TroveFM is an online store and headless CMS built with `FastAPI <https://fastapi.tiangolo.com/>`_


Links
-----

Framework Documentation: https://brianfarrell.gitlab.io/trove-fm/

License: https://www.gnu.org/licenses/agpl.html

PyPi Release: https://pypi.org/project/trove-fm/


REST API

    * Endpoint: https://trove.fm/api/

    * SwaggerUI: https://trove.fm/docs/

    * ReDoc: https://trove.fm/redoc/
