
"""
TroveFM is an online store and headless CMS.

Copyright (C) 2022  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


import os
import pathlib
import sys

import alembic
from psycopg2 import DatabaseError
from sqlalchemy import create_engine, engine_from_config, pool, text

import logging
from logging.config import fileConfig

# We're appending the 'trove_fm' directory to our path here so that we can import config easily.
# All Alembic actions will run relative to this "working directory".
sys.path.append(str(pathlib.Path(__file__).resolve().parents[4]))
from trove_fm.app.config import DATABASE_URL, POSTGRES_DB  # noqa

# Alembic Config object, which provides access to values within the .ini file
config = alembic.context.config

# Interpret the config file for logging
fileConfig(config.config_file_name)
logger = logging.getLogger("alembic.env")

logger.info(
    f"Resolved for PATH insertion: "
    f"{pathlib.Path(__file__).resolve().parents[4]}"
)


def run_migrations_online() -> None:
    """
    Run migrations in 'online' mode
    """
    DB_URL = f"{DATABASE_URL}_test" if os.environ.get("TESTING") else str(DATABASE_URL)

    # handle testing config for migrations
    if os.environ.get("TESTING"):
        # connect to primary db
        # We specify the "AUTOCOMMIT" option for isolation_level to avoid manual transaction management
        # when creating databses. Sqlalchemy always tries to run queries in a transaction, and postgres
        # does not allow users to create databases inside a transaction.
        # To get around that, we end each open transaction automatically after execution.
        # That allows us to drop a database and then create a new one inside of our default connection.
        default_engine = create_engine(str(DATABASE_URL), isolation_level="AUTOCOMMIT")
        # drop testing db if it exists and create a fresh one
        with default_engine.connect() as default_conn:
            default_conn.execute(text(f"DROP DATABASE IF EXISTS {POSTGRES_DB}_test"))
            default_conn.execute(text(f"CREATE DATABASE {POSTGRES_DB}_test"))

    connectable = config.attributes.get("connection", None)
    config.set_main_option("sqlalchemy.url", DB_URL)

    if connectable is None:
        connectable = engine_from_config(
            config.get_section(config.config_ini_section),
            prefix="sqlalchemy.",
            poolclass=pool.NullPool,
        )

    with connectable.connect() as connection:
        alembic.context.configure(
            connection=connection,
            target_metadata=None
        )
        with alembic.context.begin_transaction():
            alembic.context.run_migrations()


def run_migrations_offline() -> None:
    """
    Run migrations in 'offline' mode.
    """
    if os.environ.get("TESTING"):
        raise DatabaseError("Running testing migrations offline currently not permitted.")

    alembic.context.configure(url=str(DATABASE_URL))

    with alembic.context.begin_transaction():
        alembic.context.run_migrations()


if alembic.context.is_offline_mode():
    logger.info("Running migrations offline")
    run_migrations_offline()
else:
    logger.info("Running migrations online")
    run_migrations_online()
