
"""
TroveFM is an online store and headless CMS.

Copyright (C) 2022  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


from datetime import date
from typing import List, Optional, TypedDict
from typing_extensions import Annotated

from pydantic import constr, EmailStr, Field, HttpUrl

from trove_fm.app.models.core import CoreModel


class AddressDict(TypedDict):
    addr_label: constr(to_lower=True, min_length=3, max_length=20)
    addr_primary: bool
    addr1: constr(min_length=3, max_length=100)
    addr2: constr(max_length=100)
    city: constr(min_length=2, max_length=50)
    state: constr(min_length=2, max_length=2)
    zip_code: constr(min_length=5, max_length=5)


class EmailAddressDict(TypedDict, total=False):
    email_label: constr(to_lower=True, min_length=3, max_length=12)
    email: EmailStr
    verified: bool
    email_primary: bool


class PhoneNumberDict(TypedDict):
    phone_label: constr(max_length=12)
    phone_number: constr(max_length=15)
    phone_primary: bool


class PlatformDict(TypedDict, total=False):
    """
    docstring for PlatformDict
    """
    platform: constr(max_length=30)
    url: str
    handle: constr(max_length=30)


class URLDict(TypedDict):
    url_label: constr(max_length=30)
    url: str
    display_name: Optional[constr(max_length=30)] = None
    description: Optional[str] = None


class PersonProfileBase(CoreModel):
    dob: Optional[Annotated[date, Field(description="Date Of Birth in format 'YYYY-MM-DD'", title="Birthday")]] = None
    occupation: Optional[constr(max_length=50)] = None
    company: Optional[constr(max_length=100)] = None
    title: Optional[constr(max_length=30)] = None
    # TODO: Figure uut how to handle user images for avatars
    image_file: Optional[str] = None
    bio: Optional[str] = None
    addresses: Optional[List[AddressDict]] = None
    email_addresses: Optional[List[EmailAddressDict]] = None
    phone_numbers: Optional[List[PhoneNumberDict]] = None
    platforms: Optional[List[PlatformDict]] = None


class PersonProfileCreate(PersonProfileBase):
    """
    The only field required to create a profile is the users id
    """
    pass


class PersonProfileUpdate(PersonProfileBase):
    """
    Allow users to update any or no fields, as long as it's not user_id
    """
    pass


class PersonProfileInDB(PersonProfileBase):
    """
    Though our profiles table doesn't have a username field or an email field, we still add them
    to the PersonProfileInDB model. The PersonProfilePublic model inherits them as well.
    Depending on the situation, this may be useful for displaying user profiles in our UI.
    """
    pass


class PersonProfilePublic(PersonProfileBase):
    pass


class CompanyProfileBase(CoreModel):
    home_page: Optional[HttpUrl] = None
    logo_file: Optional[str] = None
    urls: Optional[List[URLDict]] = None


class CompanyProfileCreate(CompanyProfileBase):
    pass


class CompanyProfileInDB(CompanyProfileBase):
    pass


class CompanyProfilePublic(CompanyProfileBase):
    pass
