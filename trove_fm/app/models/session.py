
from typing import Optional

from sesh.models.key import KeyBase
from trove_fm.app.models.person import PersonInDB


class SessionData(KeyBase):
    user: Optional[PersonInDB] = None
