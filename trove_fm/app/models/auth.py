
"""
sesh is a session management library for FastAPI

Copyright (C) 2024  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


from aenum import UniqueEnum
from typing import Optional

from sesh.models.key import KeyBase

from trove_fm.app.models.person import AppRole


class UserGroup(UniqueEnum):
    ACCT = 'acct'
    CSTE = 'cste'
    HR = 'hr'
    IT = 'itech'
    MKTG = 'mktg'
    STAFF = 'staff'
    WHEEL = 'wheel'


class AuthData(KeyBase):
    user_id: Optional[int] = None
    groups: Optional[list[UserGroup]] = []
    app_role: AppRole = AppRole.GUEST
