
"""
sesh is a session management library for FastAPI

Copyright (C) 2024  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


from aenum import extend_enum

from sesh.backend.redis import RedisStore
from sesh.backend.base import SessionStore
from sesh.session.cookie import CookieSession
from sesh.models.cookie import CookieType

from trove_fm.app.config import (
    AUTH_COOKIE_SALT, AUTH_SESSION_TTL, COOKIE_DOMAIN, REDIS_DSN, SECRET_KEY, SESSION_COOKIE_SALT, SESSION_TTL
)
from trove_fm.app.models.auth import AuthData
from trove_fm.app.models.session import SessionData


for name, value in (
    ('AUTH', (AuthData, 'AuthData')),
    ('SESSION', (SessionData, 'SessionData')),
):
    extend_enum(CookieType, name, value)

redis_backend: SessionStore = RedisStore(REDIS_DSN)

auth_session = CookieSession(
    backend=redis_backend,
    cookie_type=CookieType.AUTH,
    created_by=1,
    domain=COOKIE_DOMAIN,
    max_age=AUTH_SESSION_TTL,
    salt=AUTH_COOKIE_SALT,
    secret_key=str(SECRET_KEY),
)

user_session = CookieSession(
    backend=redis_backend,
    cookie_type=CookieType.SESSION,
    created_by=1,
    domain=COOKIE_DOMAIN,
    max_age=SESSION_TTL,
    salt=SESSION_COOKIE_SALT,
    secret_key=str(SECRET_KEY),
)
