
"""
TroveFM is an online store and headless CMS.

Copyright (C) 2022  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


from typing import Optional

from fastapi import APIRouter, BackgroundTasks, Body, Depends, HTTPException, Request, Response, status
from itsdangerous.exc import BadSignature, SignatureExpired
from loguru import logger
from pydantic import EmailStr, ValidationError
from sesh.dependencies.authentication import login_check, get_state, user_auth
from sesh.exceptions import SessionError
from sesh.models.cookie import CookiePayload

from trove_fm.app.config import AUTH_SESSION_TTL, BASE_URL, COOKIE_DOMAIN, new_key_id, SESSION_TTL
from trove_fm.app.api.cookies import auth_session, user_session
from trove_fm.app.api.dependencies.database import get_repository
from trove_fm.app.db.repositories.person import PersonRepository
from trove_fm.app.db.repositories.profile import PersonProfileRepository
from trove_fm.app.exceptions import AuthFailure, UsernameExists, VerificationFailure
from trove_fm.app.models.auth import AuthData
from trove_fm.app.models.person import PersonCredentials, PersonInDB, PersonPublic, PersonUnverified
from trove_fm.app.models.profile import PersonProfileCreate
from trove_fm.app.models.session import SessionData
from trove_fm.app.services import auth_service, email_service, verification_service


router = APIRouter()


@router.post(
    "/",
    response_model=PersonUnverified,
    response_model_exclude_none=True,
    name="person:register-person-credentials",
    status_code=status.HTTP_201_CREATED
)
async def register_person_credentials(
    background_tasks: BackgroundTasks,
    new_person_creds: PersonCredentials,
    person_repo: PersonRepository = Depends(get_repository(PersonRepository)),
) -> PersonUnverified:

    try:
        created_person_creds = await person_repo.register_person_credentials(new_person_creds=new_person_creds)
    except UsernameExists:
        logger.error(f"Username {new_person_creds.username} already exists in database.")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="That email is already taken. Login with that email or register with another one."
        )
    except ValueError as e:
        logger.error(str(e))
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=str(e)
        )

    verification_url = verification_service.get_verification_url(
        created_person_creds.username, f"{BASE_URL}/api/person/verify/"
    )

    link_expiration = await email_service.send_verification(created_person_creds, verification_url, background_tasks)
    message = f"Your confirmation email has been sent to {created_person_creds.username}"
    help_message = f"Didn't get the link? Click here to resend: {BASE_URL}/person/verify/resend/"

    person_unverified = PersonUnverified(
        **created_person_creds.dict(),
        link_expiration=link_expiration,
        message=message,
        help_message=help_message
    )

    return person_unverified


@router.post(
    "/verify/resend/",
    response_model=PersonUnverified,
    response_model_exclude_none=True,
    name="person:resend-verification-email",
)
async def resend_verification(
    background_tasks: BackgroundTasks,
    username: EmailStr = Body(..., embed=True),
    person_repo: PersonRepository = Depends(get_repository(PersonRepository))
) -> PersonUnverified:
    person_in_db = await person_repo.get_person_by_username(username=username)

    if not person_in_db:
        logger.error(f"Username {username} not found in database.")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"The email {username} is not found in the database. Please register first."
        )

    if person_in_db.verified is True:
        logger.error(f"The person with username {username} is already verified.")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"The email {username} has already been verified."
        )

    verification_url = verification_service.get_verification_url(
        person_in_db.username, f"{BASE_URL}/api/person/verify/"
    )

    link_expiration = await email_service.send_verification(person_in_db, verification_url, background_tasks)
    message = f"Your confirmation email has been sent to {person_in_db.username}"
    help_message = f"Didn't get the link? Click here to resend: {BASE_URL}/person/verify/resend/"

    person_unverified = PersonUnverified(
        **person_in_db.dict(),
        link_expiration=link_expiration,
        message=message,
        help_message=help_message
    )

    return person_unverified


@router.get(
    "/verify/{confirmation_token}/",
    response_model=PersonPublic,
    response_model_exclude_none=True,
    name="person:verify",
    status_code=status.HTTP_200_OK
)
async def verify_new_registration(
    confirmation_token: str, person_repo: PersonRepository = Depends(get_repository(PersonRepository))
) -> PersonPublic:
    # TODO: Send Welcome email, confirming registration
    try:
        username = verification_service.verify_token(confirmation_token)
    except SignatureExpired:
        logger.error(f"Signature expired for token {confirmation_token}")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="The token presented has expired. Please request a new verification email."
        )
    except BadSignature:
        logger.error(f"Bad signature for token {confirmation_token}")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="The token presented has a bad signature. Please request a new verification email."
        )

    try:
        person_verified = await person_repo.verify_new_person_creds(username)
    except VerificationFailure:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"The email {username} has already been verified."
        )

    return person_verified


@router.post(
    "/login/",
    response_model=SessionData,
    response_model_exclude_none=True,
    name="person:login-email-and-password"
)
async def person_login_with_email_and_password(
    creds: PersonCredentials,
    request: Request,
    response: Response,
    person_repo: PersonRepository = Depends(get_repository(PersonRepository)),
    auth_data: dict = Depends(login_check(auth_session))
) -> SessionData:
    """
    Allow person to login
    """
    authed_person = None
    if not auth_data:
        try:
            authed_person: Optional[PersonInDB] = await person_repo.authenticate_account(
                email=creds.username, password=creds.password
            )
        except AuthFailure:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Authentication was unsuccessful."
            )
        if authed_person:
            # If the user is found in the persistent_db, they are assumed to be authorized
            # Write the user's 'auth' data to the Redis cache.
            try:
                auth_data = AuthData(
                    key_id=new_key_id(),
                    key_ttl=AUTH_SESSION_TTL,
                    **authed_person.model_dump()
                )
                await auth_session.backend.create_entry(auth_data)
            except ValidationError as e:
                raise HTTPException(status_code=422, detail=e.json(include_input=False).replace('"', ""))
            except SessionError:
                raise HTTPException(
                    status_code=503,
                    detail="There is a problem with the session validation service."
                )
        else:
            # User not found in database, so they must be a new user.
            # Ask them to sign-up.
            return RedirectResponse(url=f"http://{COOKIE_DOMAIN}/api/sign-up/")  # noqa
    else:
        # User's'auth' cookie from last visit is still valid.
        # 'Auth' data returned from cache.
        pass

    # Pack the cookie's payload and attach it to the response.
    payload = auth_session.pack_payload(
        CookiePayload(
            key_id=auth_data.key_id,
            key_ttl=auth_data.key_ttl,
            data_model=auth_session.cookie_type.model
        )
    )
    auth_session.attach_to_response(payload, response)

    # Get valid session data from the Redis cache, if it exists.
    session_data: SessionData = await user_session(request, response, login_route=True)

    if authed_person and not session_data:
        # Create new session data
        try:
            session_data: SessionData = SessionData(
                key_id=new_key_id(),
                key_ttl=SESSION_TTL,
                user=authed_person
            )
            await user_session.backend.create_entry(session_data)
        except ValidationError as e:
            raise HTTPException(status_code=422, detail=e.json(include_input=False).replace('"', ""))
        except SessionError:
            raise HTTPException(
                status_code=503,
                detail="There is a problem with the session validation service."
            )

    # Pack the cookie's payload and attach it to the response.
    payload = user_session.pack_payload(
        CookiePayload(
            key_id=session_data.key_id,
            key_ttl=session_data.key_ttl,
            data_model=user_session.cookie_type.model
        )
    )
    user_session.attach_to_response(payload, response)

    return session_data


@router.post(
    "/profile/",
    name="person:create-profile",
    response_model=PersonPublic,
    response_model_exclude_none=True,
    status_code=status.HTTP_201_CREATED
)
async def create_profile(
    new_profile: PersonProfileCreate = Body(..., embed=True),
    profile_repo: PersonProfileRepository = Depends(get_repository(PersonProfileRepository)),
    session_data: SessionData = Depends(get_state(user_session))
) -> SessionData:
    created_profile = await profile_repo.create_profile_for_person(person=session_data.user, new_profile=new_profile)
    session_data.user.profile = created_profile

    return session_data


@router.get(
    "/profile/",
    response_model=PersonPublic,
    response_model_exclude_none=True,
    name="person:get-current-person")
async def get_currently_authenticated_person(
    session_data: SessionData = Depends(get_state(user_session))
) -> PersonPublic:
    person_public = PersonPublic(**session_data.user.model_dump())

    return person_public
