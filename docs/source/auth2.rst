
Authentication and Authorization
================================

TroveFM issues authenticated users of the system a Java Web Token (JWT, pronounced "jot").

The JWT specifaction is outlined in `IETF RFC-7519 <https://datatracker.ietf.org/doc/html/rfc7519>`_ 

The definitions below are taken from the RFC.


Registered Claim Names
----------------------

TroveFM makes use of the following registered claim names:

**iss** : *str*
    The "iss" (issuer) claim identifies the principal that issued the JWT.  The processing of this claim is generally application specific.

**aud** : *str* [#f1]_
    The "aud" (audience) claim identifies the recipients that the JWT is intended for.  Each principal intended to process the JWT MUST identify itself with a value in the audience claim.  If the principal processing the claim does not identify itself with a value in the "aud" claim when this claim is present, then the JWT MUST be rejected.

**exp** : *datetime*
    The "exp" (expiration time) claim identifies the expiration time on or after which the JWT MUST NOT be accepted for processing.  The processing of the "exp" claim requires that the current date/time MUST be before the expiration date/time listed in the "exp" claim.  Implementers MAY provide for some small leeway, usually no more than a few minutes, to account for clock skew.

**iat** : *datetime*
    The "iat" (issued at) claim identifies the time at which the JWT was issued.  This claim can be used to determine the age of the JWT

**sub** : *str*
    This claim identifies the principal that is the subject of the JWT.  The claims in a JWT are normally statements about the subject.  The subject value MUST either be scoped to be locally unique in the context of the issuer or be globally unique.




.. rubric:: Footnotes

.. [#f1] | According to the RFC, the 'aud' claim is generally a list of strings, however it may be
         | represented as a string in special cases, as it is here at TroveFM.
