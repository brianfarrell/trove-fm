
"""
TroveFM is an online store and headless CMS.

Copyright (C) 2022  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com

Configuration file for the Sphinx documentation builder.

This file only contains a selection of the most common options. For a full
list see the documentation:
https://www.sphinx-doc.org/en/master/usage/configuration.html
"""


import os

from setuptools_scm import get_version

from trove_fm.app.config import BASE_URL


# -- Project information -----------------------------------------------------

project = 'TroveFM'
copyright = '2022, Brian Farrell'
author = 'Brian Farrell'

if not (release := os.getenv("DOCS_VERSION")):
    # The full version, including alpha/beta/rc tags
    release = get_version(root='../..', relative_to=__file__)

lock = os.getenv("DOCS_LOCK", "False")

if lock == "True":
    release = '.'.join(release.split('.')[:3])

version = '.'.join(release.split('.')[:3])


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
# EXCLUDED 'sphinx.ext.ifconfig' for now.
extensions = [
    'sphinx_material',
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx.ext.viewcode',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.extlinks'
]

DOC_BASE_URL = os.getenv("DOC_BASE_URL")
extlinks = {'base_url': (DOC_BASE_URL + '%s', DOC_BASE_URL)}

#autodoc options
#autodoc_default_options = {
#    'exclude-members': 'test'
#}

# Napoleon settings
napoleon_google_docstring = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
source_suffix = '.rst'

# The root document.
root_doc = 'index'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_material'

# Material theme options (see theme.conf for more information)
html_theme_options = {

    # Set the name of the project to appear in the navigation.
    'nav_title': 'TroveFM',

    # Set you GA account ID to enable tracking
    # 'google_analytics_account': 'UA-XXXXX',

    # Specify a base_url used to generate sitemap.xml. If not
    # specified, then no sitemap will be built.
    'base_url': 'https://brianfarrell.gitlab.io/trove-fm/',

    # Set the color and the accent color
    'color_primary': 'green',
    'color_accent': 'purple',

    # Set the repo location to get a badge with stats
    'repo_type': 'gitlab',
    'repo_url': 'https://gitlab.com/brianfarrell/trove-fm/',
    'repo_name': 'TroveFM',

    # Visible levels of the global TOC; -1 means unlimited
    'globaltoc_depth': 2,
    # If False, expand all TOC entries
    'globaltoc_collapse': False,
    # If True, show hidden TOC entries
    'globaltoc_includehidden': False,

    "version_dropdown": True,
    "version_json": "_static/versions.json",
}

html_logo = '_static/logo.svg'

html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# These paths are either relative to html_static_path
# or fully qualified paths (eg. https://...)
# html_css_files = [
#     'css/custom.css',
# ]
