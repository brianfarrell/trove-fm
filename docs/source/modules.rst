TroveFM Code Docs
=================

**NOTE:**

The documentation below is for the underlying TroveFM CMS framework.  It will help
developers to understand the inner-workings of the TroveFM code base.

Most developers will probably be more interested in the TroveFM API.

The TroveFM API is documented seperately, and is available in two formats:

   * Swagger: :base_url:`/docs/`
   * ReDoc: :base_url:`/redoc/`

If you are *still* interested in the documentation for the code base, by all means,
dig in:

.. toctree::
   :maxdepth: 5

   trove_fm
