
Setting Up Your Account
=======================

Your email address will be your username.  You do not need an email address to be entered as a contact in the system,
but you *do need* an email address to be able to login to the system.

App Roles
---------
* Admin
* Customer
* Power
* Guest
* Staff
* Vendor

Power roll can add new users and grant any app role other than `power` or `admin`

Account Creation
----------------

Contact Creation
----------------
A contact may be created through one of two methods, or a combination of the two:

A contact may first enter the system via the CMS interface.  Perhaps they are a business contact made outside of
the app.  In this case, a staff member will enter whatever information they have.

**Required Fields Include:**

* Either a First or Last Name (Preferrably both, but only one is required)
* The ID of the staff member who made the entry

**Optional Fields Include:**

* Physical Addresses
* Email Addresses
* Phone Numbers
* A referral code that the staff member provided to the contact
* Notes

The staff member who enters this information will **not** be allowed to create an online account for this new contact,
however, they will have the option to send an email to the contact, inviting them to sign-up online.

A contact may first enter the system by signing-up for an online account.  This may be the first contact that the
business has with this person.

**Required Fields Include:**

* A username, provided as a verifiable email address
* A password, validated for sufficient complexity
* Either a First or Last Name (Preferrably both, but only one is required)

**Optional Fields Include:**

* A referral code provided by a staff member

An online account will not be accessible until the email address provided for the username is verified.

*Upon verification of the email*, a user will be prompted for the following:

* The opportunity to turn-on 2FA
* The opportunity to fill-in their profile

When a person signs-up for an online account, they will initially be assigned an app role of 'guest'.

When they verify their email, their app role will be upgraded to 'customer'
