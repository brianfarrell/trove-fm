===============
Getting Started
===============

Trove Farmer's Market (TroveFM) offers a simple but effective platform for small businesses to create an online store,
manage their contacts, and market their offerings.

TroveFM makes it easy to begin, and offers plenty of ways to grow as you grow your business.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   account_setup
